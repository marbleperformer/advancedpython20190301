import json
import socket
import logging
import select
from datetime import datetime

from protocol import (
    validate_request, make_response, 
    make_400, make_404
)
from routes import resolve
from utils import get_client_fullname
from handelers import handle_client_request
from settings import (
    HOST, PORT
)


error_handler = logging.FileHandler('error.log') 
error_handler.setLevel(logging.CRITICAL)

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.StreamHandler(),
        error_handler
    ]
)

requests = []
connections = []

try:
    sock = socket.socket()
    sock.bind((HOST, PORT))
    sock.listen(5)

    logging.info(f'Server start with host:{ HOST } and port: { PORT }')
    
    while True:
        client, address = sock.accept()
        client_full_name = get_client_fullname(*address)
        connections.append((client_full_name, client))
        logging.info(f'Client detected { client_full_name }')

        client_sockets = list(map(lambda item: item[1], connections))

        rlist, wlist, xlist = select.select(
            client_sockets, client_sockets, [], 0
        )

        for client in wlist:
            read_client_host, read_client_port = client.getsockname()
            read_client_fullname = get_client_fullname(
                read_client_host, 
                read_client_port
            )
            data = client.recv(1024)
            request = json.loads(data.decode('utf-8'))
            requests.append((read_client_fullname, request))

        print(requests)
    
        if requests:
            request_client_fullname, request = requests.pop()
            response = handle_client_request(request)

            print(request_client_fullname, request)

            for client in rlist:
                print('2'*50)
                write_client_host, write_client_port = client.getsockname()
                write_client_fullname = get_client_fullname(
                    write_client_host,
                    write_client_port
                )

                if write_client_fullname != write_client_fullname:
                    response_string = json.dumps(response)
                    client.send(response_string.encode('utf-8'))
                    logging.info(
                        f'Response { response_string } sended to {client.getsockname()}'
                    )

except KeyboardInterrupt:
    logging.info('Shutdown server')
    sock.close()
