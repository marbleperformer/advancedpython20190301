import logging
from routes import resolve

from protocol import (
    validate_request, make_response,
    make_400, make_404
)

def handle_client_request(request):
    action_name = request.get('action')
    if validate_request(request):
        controller = resolve(action_name)
        if controller:
            try:
                return controller(request)
            except Exception as err:
                logging.critical(err, exc_info=True)
                return make_response(
                    request, 500,
                    'Internal server error.'
                )
        else:
            logging.error(f'Action not found: { action_name }')
            return make_404(request)
    else:
        logging.error(f'Bad Request: { action_name } request: { request }')
        return make_400(request)
