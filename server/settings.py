import os

HOST = 'localhost'

PORT = 8000

BASE_DIR = os.path.dirname(
    os.path.abspath(__file__)
)

INSTALLED_MODULES = [
    'text',
    'exception'
]
